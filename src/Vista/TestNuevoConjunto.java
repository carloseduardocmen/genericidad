/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Conjunto;

/**
 *
 * @author MADARME
 */
public class TestNuevoConjunto {
    
    public static void main(String[] args) {
     try{  
        
        Conjunto<Integer> c1=new Conjunto(2);
        Conjunto<Integer> c2=new Conjunto(4);
        
        
        //probar todos los métodos públicos(imprimir)
        //Recomendación: Realicen método para crear cada una de las pruebas.

           c1.adicionarElemento(5);
           c1.adicionarElemento(2);
           c2.adicionarElemento(6);
           c2.adicionarElemento(4);
           c2.adicionarElemento(1);
           c2.adicionarElemento(3);
           
           System.out.println("Método Ordenar");  
            c1.ordenar();
            System.out.println(c1.toString());
            System.out.println(c1.toString());
            
            System.out.println("Método Ordenar Burbuja");
            c2.ordenarBurbuja();
            System.out.println(c2.toString());
            c2.remover(6);
            
            System.out.println("Método Remover"); 
            c2.remover(4);
            System.out.println(c2.toString());
            c2.remover(1);
            System.out.println(c2.toString());
            
            System.out.println("Método Concatenar");
            c1.concatenar(c2);
            System.out.println(c1.toString());
           
            System.out.println("Método Concatenar Restrictivo");
            c1.concatenarRestrictivo(c2);
            System.out.println(c1.toString());

           
           
      }
       catch(Exception ex)
       {
        System.err.println(ex.getMessage());
       }
       
    }
}
