/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 * Clase contenedora: Cada uno de sus elementos con cajas parametrizadas
 * Conjunto es una estructura da datos estática
 * @author MADARME
 */
public class Conjunto<T> {
    //Estructura de datos estática
    private Caja<T> []cajas;
    private int i=0;
    
    public Conjunto(){}
    
    public Conjunto(int cantidadCajas)
    {
    if(cantidadCajas <=0)
           throw new RuntimeException("No se pueden crear el Conjunto");
        
     this.cajas=new Caja[cantidadCajas];
    }
    
    
    public void adicionarElemento(T nuevo) throws Exception
    {
        if(i>=this.cajas.length)
            throw new Exception("No hay espacio en el Conjunto");
        
        if(this.existeElemento(nuevo))
            throw new Exception("No se puede realizar inserción, elemento repetido");
        
        
        this.cajas[i]=new Caja(nuevo);
        this.i++;
    
    }
    
    public T get(int indice)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        return this.cajas[indice].getObjeto();
            
    }
    
    
    public int indexOf(T objBuscar)
    {
    
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(objBuscar))
                return j;
        }
        
        return -1;
        
    }
    
    public void set(int indice, T nuevo)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        this.cajas[indice].setObjeto(nuevo);
            
    }
    
    
    public boolean existeElemento(T nuevo)
    {
        
        //Sólo estoy comparando por los estudiantes matriculados
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(nuevo))
                return true;
        }
        
        return false;
    
    }
    
    
    /**
     *  para el grupo A--> Selección
     *  para el grupo C--> Inserción
     * 
     */
    public void ordenar()
    {
     int j;
     Comparable ordenado;
     for(int i=1; i<this.cajas.length; i++)
     {
      ordenado = (Comparable)cajas[i].getObjeto();
      j = i - 1;
      while((j>=0)&&(ordenado.compareTo(cajas[j].getObjeto())<0))
      {
       cajas[j+1] = cajas[j];
       j--;
      }
       cajas[j+1] = (Caja)ordenado;
     }// :)
    }
    
    
    /**
     * Realiza el ordenamiento por burbuja 
     */
    public void ordenarBurbuja()
    {
     Caja ordenado;
     for(int i=0; i<this.cajas.length-1; i++)
     {
      int j = 0;
      while( j<this.cajas.length-i-1)
      {   
       j++;
       Comparable temporal = (Comparable)cajas[j+1].getObjeto();
       if(temporal.compareTo(cajas[j].getObjeto()) < 0)
       {//se pasan ordenados al auxiliar y se iguala al original
        ordenado = cajas[j+1];
        cajas[j+1] = cajas[j];
        cajas [j] = ordenado;
       }
      }
     }   
    }
    
    /**
     * Elimina un elemento del conjunto y compacta
     * @param objBorrado es el objeto que deseo eliminar
     */
    
    public void remover(T objBorrado)
    {
     Caja<T> []temporal = new Caja [this.cajas.length-1];
     int j = 0;
     for(int i=0; i<this.cajas.length; i++)
     {
      if(objBorrado.equals(this.cajas[i]) == false)
       {
        temporal[j].setObjeto(this.cajas[i].getObjeto());
        j++;
       }
     }
      int k = this.indexOf(objBorrado);
      for(int l=k;l<this.getCapacidad()-1;l++)
      {
       T aux=this.get(l);
       this.set(l, this.get(l+1));
       this.set(l+1, aux);

      }
       this.cajas = temporal; 
    }
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío. En este proceso no se toma en cuenta los datos repetidos
     * Ejemplo:
     *  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2)
     *  da como resultado: conjunto1=<1,2,3,5,6,9,1,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenar(Conjunto<T> nuevo)
    {
     Caja<T> [] conjunto = new Caja [this.getCapacidad() + nuevo.getCapacidad()];
     for(int j=0; i<conjunto.length; j++)
      {
       if(j<this.getCapacidad())
        {
         conjunto[j].setObjeto(this.cajas[j].getObjeto());
        }
         else
        {
         conjunto[j].setObjeto(nuevo.cajas[j - this.getCapacidad()].getObjeto());
        }
      }
     nuevo.removeAll();
     this.cajas = conjunto;

    }
    
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío. En este proceso SI toma en cuenta los datos repetidos
     * Ejemplo:
     *  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2)
     *  da como resultado: conjunto1=<1,2,3,5,6,9,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenarRestrictivo(Conjunto<T> nuevo)
    {
        this.concatenar(nuevo);
        for(int i=0; i<this.cajas.length-1; i++)
        {
         Caja<T> conjunto = cajas[i];
         for(int j = i+1; j<this.cajas.length; j++)
         {
          if(conjunto.equals(cajas[j]))
          cajas[j].remover();
         }
        }
        quitarEspacios();
    }
    
    public void quitarEspacios(){
        int count=0; 
        for(int i=0; i<this.cajas.length; i++)
        if(cajas[i]==null)
        {    
         count++;
        }
         Caja<T> []tmp = new Caja [this.cajas.length - count];
         int j = 0;
         for(int k=0; k<this.cajas.length; k++)
         {    
          if(this.cajas[k] != null)
          {    
           tmp[j].setObjeto(cajas[k].getObjeto());
          } 
         }
        this.cajas = tmp;
    }

    
    public void removeAll()
    {
        this.cajas=null;
        this.i=0;
    }
    
    
    @Override
    public String toString() {
        String msg="******** CONJUNTO*********\n";
        
        for(Caja c:this.cajas)
            msg+=c.getObjeto().toString()+"\n";
        
        return msg;
    }
    
    
    
    /**
     * Obtiene la cantidad de elementos almacenados
     * @return  retorna un entero con la cantidad de elementos
     */
    public int getCapacidad()
    {
        return this.i;
    }
    
    /**
     *  Obtiene el tamaño máximo de cajas dentro del Conjunto
     * @return int con la cantidad de cajas
     */
    public int getLength()
    {
        return this.cajas.length;
    }
    
    /**
     * Obtiene el mayor elemento del Conjunto, recordar que el conjunto no posee elementos repetidos.
     * @return el elemnto mayor de la colección
     */
    public T getMayorElemento()
    {
    if(this.cajas==null)
        throw new RuntimeException("No se puede encontrar elemento mayor, el conjunto está vacío");
    
    T mayor=this.cajas[0].getObjeto();
    for(int i=1;i<this.getCapacidad();i++)
    {
        //Utilizo la interfaz comparable y después su método compareTo
        Comparable comparador=(Comparable)mayor;
        T dato_A_comparar=this.cajas[i].getObjeto();
        // Resta entra mayor-datoAComparar 
        int rta_compareTo=comparador.compareTo(dato_A_comparar);
        if(rta_compareTo<0)
            mayor=dato_A_comparar;
    }
    return mayor;
    }
}
